#include "PrinterClass.hpp"
#include <iostream>


PrinterClass::PrinterClass(const std::string & text) : mText(text) {

}

void PrinterClass::printTextToConsole(){
    std::cout << mText << std::endl;
}