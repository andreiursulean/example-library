#pragma once

#include <string>

class PrinterClass {
    std::string mText;
public:
    PrinterClass(const std::string & text);

    void printTextToConsole(); 
};