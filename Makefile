SUBDIRS = $(shell find src/ -type d)
PROFILE = Release

BASE_FLAGS = -c -fPIC --std=c++17 -MMD -I. $(addprefix -I, $(SUBDIRS))

ifeq ($(PROFILE), Release)
CXXFLAGS = $(BASE_FLAGS) -O3 
else ifeq ($(PROFILE), Debug)
CXXFLAGS = $(BASE_FLAGS) -Og -g
endif

CPP_FILES = $(shell find src/ -name "*.cpp")
OBJ_FILES = $(CPP_FILES:src/%.cpp=$(PROFILE)/%.o)
D_FILES = $(CPP_FILES:src/%.cpp=$(PROFILE)/%.d)
LDFLAGS = --shared
LIB_NAME = libdep1.so
STATIC_LIB_NAME = libdep1.a

.PHONY: all
all: $(PROFILE)/$(LIB_NAME) $(PROFILE)/$(STATIC_LIB_NAME)

$(PROFILE)/$(LIB_NAME): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS)

$(PROFILE)/$(STATIC_LIB_NAME): $(OBJ_FILES)
	ar rvs  $@ $^

$(PROFILE)/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $< -o $@

-include $(D_FILES)

.PHONY: install uninstall
install: $(PROFILE)/$(LIB_NAME)
	mkdir -p /usr/share/library1
	cp $< /usr/share/library1/.
	mkdir -p /usr/include/library1
	cp $(shell find src/ -name "*.hpp") /usr/include/library1/.
	echo /usr/share/library1 > /etc/ld.so.conf.d//library1.conf
	ldconfig

uninstall: $(PROFILE)/$(LIBNAME)
	rm -rf /usr/share/library1 /usr/include/library1

.PHONY: clean distclean
clean: 
	rm -rf $(PROFILE)

distclean:
	rm -rf Debug Release